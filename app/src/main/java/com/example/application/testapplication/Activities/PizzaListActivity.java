package com.example.application.testapplication.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.application.testapplication.Adapter.PizzaAdapter;
import com.example.application.testapplication.ExternalUtils.ConnectionUtils;
import com.example.application.testapplication.ExternalUtils.KeyConstants;
import com.example.application.testapplication.JobService.MyJobService;
import com.example.application.testapplication.POJOs.Pizza;
import com.example.application.testapplication.R;
import com.example.application.testapplication.Retrofit.ApiClient;
import com.example.application.testapplication.Retrofit.ApiEndPoint;
import com.example.application.testapplication.Services.MyBroadcastReceiverService;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PizzaListActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private PizzaAdapter pizzaAdapter;
    private Button clickButton,addJobButton,cancelJobButton;
    private FirebaseJobDispatcher firebaseJobDispatcher;
    private static final String JOB_TAG = "MyJob";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_list);
        clickButton = (Button) findViewById(R.id.button);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPizza();
            }
        });
        addJobButton = findViewById(R.id.buttonAdd);
        cancelJobButton = findViewById(R.id.buttonCancel);

        addJobButton.setOnClickListener(this);
        cancelJobButton.setOnClickListener(this);

        init();
        initView();
        populatePizza();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MyBroadcastReceiverService.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(),23432423,intent,0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+(10*1000),pendingIntent);
        Toast.makeText(this,"Alarm set in " +1+" seconds",Toast.LENGTH_LONG).show();
    }

    public void init() {
        pizzaAdapter = new PizzaAdapter(this);
    }


    public void addPizza(){
        Pizza pizza = new Pizza();
        pizza.setId(Long.valueOf(7777));
        pizza.setPrice(Double.valueOf(7777));
        pizza.setName("Mayuresh");
        pizza.setDescription("Nimbalkar");
        pizza.setVeg(false);
        pizza.setImageUrl("https://www.google.co.in/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwie9fDt14_cAhXLRo8KHZTzCTQQjRx6BAgBEAU&url=https%3A%2F%2Fwww.sopranosnm.com%2F&psig=AOvVaw3K_eVvP6e-lNdwpJVW8_KF&ust=1531145559729784");

        Retrofit retrofit = ApiClient.getRetrofitClient();
        ApiEndPoint apiEndPoint = retrofit.create(ApiEndPoint.class);

        Call<Pizza> addPizza = apiEndPoint.postOrder(pizza);

        addPizza.enqueue(new Callback<Pizza>() {
            @Override
            public void onResponse(Call<Pizza> call, Response<Pizza> response) {
                if (response.code()== KeyConstants.GET_OK) {
                    populatePizza();
                    Pizza pizza = response.body();
                    Log.d("GSON","{"+"\n"+"Name :"+pizza.getName()+",\n"+"Description :"+pizza.getDescription()+",\n"+"Price :"+pizza.getPrice()+",\n"+"ID :"+pizza.getId()+",\n"+"imageUrl :"+pizza.getImageUrl()+",\n"+"isVeg :"+pizza.getVeg()+"\n}");
                }
                else {
                    Log.e("Error in network call",response.errorBody().toString());
                }

            }

            @Override
            public void onFailure(Call<Pizza> call, Throwable t) {

            }
        });
    }

    private void populatePizza() {
        if(ConnectionUtils.isNetworkAvailable(this)){
            /*new GetPizzaData().execute();*/
            Retrofit retrofit = ApiClient.getRetrofitClient();
            ApiEndPoint apiEndPoint = retrofit.create(ApiEndPoint.class);

            Call<List<Pizza>> pizzaCall=apiEndPoint.getPizzas();

            pizzaCall.enqueue(new Callback<List<Pizza>>() {
                @Override
                public void onResponse(Call<List<Pizza>> call, Response<List<Pizza>> response) {
                    if (response.code()==KeyConstants.GET_OK){
                        List<Pizza> pizzaList = response.body();
                        pizzaAdapter.addPizzas(pizzaList);
                    }
                    else {
                        Log.e("Error in network call",response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call<List<Pizza>> call, Throwable t) {

                }
            });
        }
        else {
            Toast.makeText(this,"No internet Connection",Toast.LENGTH_LONG).show();
        }

    }

    public void initView() {
        recyclerView = findViewById(R.id.rvPizzaList);
        recyclerView.setAdapter(pizzaAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAdd:{
                createJob();
                break;
            }
            case R.id.buttonCancel:{
                cancelJob();
                break;
            }
        }
    }

    private void createJob(){
        firebaseJobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        //int periodicity = (int)

        Job  constrainRemainderJob = firebaseJobDispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag(JOB_TAG)
                .setConstraints(Constraint.DEVICE_CHARGING)
                .setLifetime(Lifetime.FOREVER)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(10,20))
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .build();

            firebaseJobDispatcher.schedule(constrainRemainderJob);
    }

    private void cancelJob() {
        if (firebaseJobDispatcher != null)
            firebaseJobDispatcher.cancel(JOB_TAG);
    }

    public class GetPizzaData extends AsyncTask<Void, Integer, Integer> {
        private List<Pizza> pizzaList;
        private ProgressDialog progressDialog = null;
        private int contentLength;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(PizzaListActivity.this);
                progressDialog.setMessage("Please wait");
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
            }
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d("Progress dialog",String.valueOf(values[0]/contentLength));
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Gson gson = new Gson();
            HttpURLConnection urlConnection;
            int responseCode = 0;
            BufferedReader bufferedReader = null;
            try {
                urlConnection = ConnectionUtils.getGetConnection(KeyConstants.ROOT_URL + KeyConstants.GET_PIZZA);
                responseCode = urlConnection.getResponseCode();
                if (responseCode == KeyConstants.GET_OK) {
                    contentLength=urlConnection.getContentLength();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line = bufferedReader.readLine();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (line != null) {
                        stringBuffer.append(line);
                        publishProgress(stringBuffer.length());
                        line = bufferedReader.readLine();
                    }
                    pizzaList = gson.fromJson(stringBuffer.toString(), new TypeToken<List<Pizza>>() {
                    }.getType());
                }
            } catch (MalformedURLException e) {
                responseCode = KeyConstants.MALFORM_URL;
                Log.e(getClass().getSimpleName(), "Exception : ", e);
            } catch (IOException e) {
                responseCode = KeyConstants.IO_ERROR;
                Log.e(getClass().getSimpleName(), "Exception : ", e);
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        Log.e(getClass().getSimpleName(), "Exception : ", e);
                    }
                }
            }
            return responseCode;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (progressDialog.isShowing() && progressDialog != null)
                progressDialog.dismiss();
            if (integer == KeyConstants.GET_OK)
                pizzaAdapter.addPizzas(pizzaList);
        }
    }

}

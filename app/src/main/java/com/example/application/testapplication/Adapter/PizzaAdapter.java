package com.example.application.testapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.application.testapplication.POJOs.Pizza;
import com.example.application.testapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.PizzaViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Pizza> pizzaList;

    public PizzaAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        pizzaList = new ArrayList<>();
    }

    public void addPizzas(List<Pizza> pizzaList){
        this.pizzaList.clear();
        this.pizzaList.addAll(pizzaList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PizzaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.pizza_line_item, parent, false);
        return new PizzaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaViewHolder holder, int position) {
        Pizza pizza = pizzaList.get(position);
        holder.bind(pizza);
    }

    @Override
    public int getItemCount() {
        return pizzaList.size();
    }

    public class PizzaViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewPizza;
        private TextView textViewPizzaName;
        private TextView textViewPizzaDesc;
        private TextView textViewPrice;
        private Button buttonAdd;

        public PizzaViewHolder(View itemView) {
            super(itemView);
            imageViewPizza=itemView.findViewById(R.id.imageViewPizza);
            textViewPizzaName=itemView.findViewById(R.id.textViewPizzaName);
            textViewPizzaDesc=itemView.findViewById(R.id.textViewPizzaContent);
            textViewPrice=itemView.findViewById(R.id.textViewRuppes);
            buttonAdd=itemView.findViewById(R.id.buttonAdd);
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getAdapterPosition();
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }

        public void bind(Pizza pizza) {
            textViewPizzaName.setText(pizza.getName());
            textViewPizzaDesc.setText(pizza.getDescription());
            textViewPrice.setText(String.valueOf(pizza.getPrice()));
            Picasso.get().load(pizza.getImageUrl()).into(imageViewPizza);
        }
    }

}

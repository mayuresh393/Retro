package com.example.application.testapplication.ExternalUtils;

public class KeyConstants {
    public static final int GET_OK = 200;
    public static final int POST_OK = 201;
    public static final String ROOT_URL = "http://10.42.0.1:8080/api/";
    public static final String GET_PIZZA = "public/pizzas";
    public static final int MALFORM_URL = 211;
    public static final int IO_ERROR = 212;
}

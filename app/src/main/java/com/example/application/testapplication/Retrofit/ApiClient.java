package com.example.application.testapplication.Retrofit;

import com.example.application.testapplication.ExternalUtils.KeyConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final int TIME_OUT = 10;
    private static final int CONNECT_TIME_OUT = 10;
    private static final int WRITE_TIME_OUT = 10;
    private static ApiEndPoint apiEndPoint = null;

    public static ApiEndPoint getApi(){
        if (apiEndPoint == null){
            Retrofit retrofit = getRetrofitClient();
            apiEndPoint = retrofit.create(ApiEndPoint.class);
        }
        return apiEndPoint;
    }

    public static Retrofit getRetrofitClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(KeyConstants.ROOT_URL)
                .client(getNormalOkHttpClient().build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static OkHttpClient.Builder getNormalOkHttpClient() {
        OkHttpClient.Builder normalOkHttpClient =  new OkHttpClient.Builder()
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIME_OUT,TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIME_OUT,TimeUnit.SECONDS);

        return normalOkHttpClient;
    }
}

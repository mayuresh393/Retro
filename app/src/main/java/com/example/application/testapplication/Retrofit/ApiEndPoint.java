package com.example.application.testapplication.Retrofit;

import com.example.application.testapplication.POJOs.Pizza;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEndPoint {
    @GET("public/pizzas")
    Call<List<Pizza>> getPizzas();

    @POST("public/pizzas")
    Call<Pizza> postOrder(@Body Pizza pizza);
}
